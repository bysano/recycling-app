part of 'welcome_bloc.dart';

@immutable
abstract class WelcomeState {}

class WelcomeInitial extends WelcomeState {
  final String testValue;

  WelcomeInitial(this.testValue);
}
