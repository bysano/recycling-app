import 'dart:async';

import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:recycling_app/global_provider.dart';
import 'package:recycling_app/log.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'feature/athintication/authentication_bloc.dart';

Future<void> bootstrap(FutureOr<Widget> Function() builder) async {

  await runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      await EasyLocalization.ensureInitialized();

      final prefs = await SharedPreferences.getInstance();
      // some initialization code

      // FlutterError.onError = (details) => Log.wtf(details.exceptionAsString(), '', details.stack);
      SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
      var globalProvider = GlobalProvider();
      // Bloc.observer = GlobalBlocObserver();
      runApp(await globalProvider.init(builder));
    },
    (error, stackTrace) => Log.e(error.toString(), '', stackTrace),
  );
}
