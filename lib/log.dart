import 'package:logger/logger.dart' as lib;

abstract class Log {
  Log._();

  static final _logger = lib.Logger();

  static void i(dynamic message, [dynamic error, StackTrace? stackTrace]) => _logger.i(message, error, stackTrace);

  static void wtf(dynamic message, [dynamic error, StackTrace? stackTrace]) => _logger.wtf(message, error, stackTrace);

  static void w(dynamic message, [dynamic error, StackTrace? stackTrace]) => _logger.w(message, error, stackTrace);

  static void e(dynamic message, [dynamic error, StackTrace? stackTrace]) => _logger.e(message, error, stackTrace);
}
