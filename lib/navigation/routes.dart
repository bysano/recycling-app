import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:recycling_app/main.dart';

import '../feature/welcome/welcome_bloc.dart';

part 'routes.g.dart';

CustomTransitionPage<T> buildPageWithDefaultTransition<T>({
  required BuildContext context,
  required GoRouterState state,
  required Widget child,
}) {
  return CustomTransitionPage<T>(
    key: state.pageKey,
    child: child,
    transitionsBuilder: (context, animation, secondaryAnimation, child) => SlideTransition(
      position: state.extra != true
          ? Tween<Offset>(begin: const Offset(1, 0), end: const Offset(0, 0)).animate(animation)
          : Tween<Offset>(begin: const Offset(-1, 0), end: const Offset(0, 0)).animate(animation),
      child: child,
    ),
  );
}

@TypedShellRoute<ShellRoute>(
  routes: <TypedRoute<RouteData>>[
    TypedGoRoute<LandingPageRoute>(path: '/'),
  ],
)
class ShellRoute extends ShellRouteData {
  const ShellRoute();

  @override
  Widget builder(BuildContext context, GoRouterState state, Widget navigator) {
    return Scaffold(
      key: state.pageKey,
      body: navigator,
    );
  }
}

class LandingPageRoute extends GoRouteData {
  LandingPageRoute({this.$extra});

  final bool? $extra;

  @override
  CustomTransitionPage<void> buildPage(BuildContext context, GoRouterState state) {
    return buildPageWithDefaultTransition(
      child: BlocProvider<WelcomeBloc>(
        child: const MyHomePage(title: 'Flutter Demo Home Page'),
        create: (context) => WelcomeBloc(),
      ),
      context: context,
      state: state,
    );
  }
}
