import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

import '../feature/athintication/authentication_bloc.dart';
import 'routes.dart';


abstract class AppRouter extends StatefulWidget {
  final Widget Function(GlobalKey? navigatorKey, GoRouter router) builder;

  factory AppRouter.create({
    required Widget Function(GlobalKey? navigatorKey, GoRouter router) builder,
    Key? key,
  }) =>
      _AppRouter(builder: builder, key: key);

  const AppRouter({super.key, required this.builder});

}

class _AppRouter extends AppRouter {
  const _AppRouter({super.key, required super.builder});

  @override
  State<_AppRouter> createState() => _AppRouterState();
}

class _AppRouterState extends State<_AppRouter> {
  late final GoRouter router;

  final _kRoot = GlobalKey<NavigatorState>();

  @override
  void initState() {
    initRouter();
    super.initState();
  }

  void initRouter() => router = GoRouter(
        navigatorKey: _kRoot,
        initialLocation: '/',
        routes: $appRoutes,
        redirect: onRedirect,
      );

  String? onRedirect(BuildContext context, GoRouterState state) {
    final authBloc = context.read<AuthenticationBloc>();

    // example for auth flow

    // final status = authBloc.state.status;
    // final bool isOnLoginPage = state.location == Routes.login;
    // final bool isOnEntryPage = state.location == Routes.entry;
    //
    // final redirectUrl = status == SessionStatus.undefined
    //     ? null
    //     : status == SessionStatus.active
    //         ? onAuthorizedRedirect(context, isOnLoginPage, isOnEntryPage)
    //         : onUnauthorizedRedirect(isOnLoginPage);
    //
    // if (redirectUrl != null) {
    //   Log.i('Redirecting to < $redirectUrl >, current location is < ${state.location} >');
    // }

    return '/';
  }

  // String? onUnauthorizedRedirect(bool isOnLoginPage) => isOnLoginPage ? null : Routes.login;
  //
  // String? onAuthorizedRedirect(BuildContext context, bool isOnLoginPage, bool isOnEntryPage) {
  //   String? redirectUrl;
  //
  //   if (isOnLoginPage || isOnEntryPage) {
  //     redirectUrl = Routes.home;
  //   }
  //
  //   return redirectUrl;
  // }

  /// Should trigger [onRedirect] check
  void refreshRouter() => router.refresh();

  List<BlocListener> get refreshListeners => [
        BlocListener<AuthenticationBloc, AuthenticationState>(listener: (_, __) => refreshRouter()),
      ];

  @override
  Widget build(BuildContext context) => MultiBlocListener(
        listeners: refreshListeners,
        child: widget.builder(_kRoot, router),
      );
}
