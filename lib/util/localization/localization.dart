import 'dart:ui';


import 'package:easy_localization/easy_localization.dart';

import 'localization.g.dart';

export 'package:easy_localization/easy_localization.dart';

enum AppLocales { en, es }

extension LocalizationExtension on AppLocales {
  Locale get value => Locale(name);

  String get localizedName {
    switch (this) {
      case AppLocales.en:
        return LK.profile_english.tr();
      case AppLocales.es:
        return LK.profile_spanish.tr();
    }
  }
}

final supportedLocales = AppLocales.values.map((l) => l.value).toList();
const translationsPath = 'assets/translations';

/// short access for [LocaleKeys]
typedef LK = LocaleKeys;
