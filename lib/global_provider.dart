import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'feature/athintication/authentication_bloc.dart';


typedef AppRootBuilder = FutureOr<Widget> Function();

class GlobalProvider {


  GlobalProvider();


  Future<Widget> init( AppRootBuilder builder,) async {
    return MultiProvider(
      providers: [
        Provider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(),
          lazy: false,
        ),
      ],
      child: await builder(),
    );
  }
}

