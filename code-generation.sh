echo "Generating code gen files and rebuilding translations"
flutter pub run easy_localization:generate -f keys -O lib/util/localization -f keys -o localization.g.dart --source-dir ./assets/translations
