# recycling_app

A new Flutter project.

## Getting Started

## Localization
for using localization i used this package [easy_localization](https://pub.dev/packages/easy_localization)
there are localization files in assets/translations
after adding new localization key pair value you should run this command:

flutter pub run easy_localization:generate -f keys -O lib/util/localization -f keys -o localization.g.dart --source-dir ./assets/translations

then you can use it like this:
LK.hi.tr()

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
